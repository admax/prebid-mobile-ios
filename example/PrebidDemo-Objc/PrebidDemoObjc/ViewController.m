/*   Copyright 2018-2019 Prebid.org, Inc.
 
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
 http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 */

@import GoogleMobileAds;
@import AdmaxPrebidMobile;

#import "ViewController.h"
#import "MoPub.h"
#import "DemoAdmaxExceptionLogger.h"

@interface ViewController () <GADBannerViewDelegate,MPAdViewDelegate,MPInterstitialAdControllerDelegate, GADAppEventDelegate, AdSizeDelegate>
    @property (weak, nonatomic) IBOutlet UIView *bannerView;
    @property (nonatomic, strong) GAMBannerView *dfpView;
    @property (nonatomic, strong) GAMInterstitialAd *dfpInterstitial;
    @property (nonatomic, strong) GAMRequest *request;
    @property (nonatomic, strong) MPAdView *mopubAdView;
    @property (nonatomic, strong) MPInterstitialAdController *mopubInterstitial;
    @property (nonatomic, strong) GamBannerAdUnit *bannerUnit;
    @property (nonatomic, strong) GamInterstitialAdUnit *interstitialUnit;
    
    @end

@implementation ViewController
    
- (void)viewDidLoad {
    [super viewDidLoad];
    
    Prebid.shared.prebidServerAccountId = @"4803423e-c677-4993-807f-6a1554477ced";
    Prebid.shared.prebidServerHost = PrebidHostAdmax;
    Prebid.shared.shareGeoLocation = true;
    Prebid.shared.loggingEnabled = true;
    Prebid.shared.admaxExceptionLogger = [[DemoAdmaxExceptionLogger alloc] init];
    [Prebid.shared initAdmaxConfig];
    
    if([self.adServer isEqualToString:@"DFP"] && [self.adUnit isEqualToString:@"Banner"])
    [self loadDFPBanner];
    if([self.adServer isEqualToString:@"DFP"] && [self.adUnit isEqualToString:@"Interstitial"])
    [self loadDFPInterstitial];
    if([self.adServer isEqualToString:@"MoPub"] && [self.adUnit isEqualToString:@"Banner"])
    [self loadMoPubBanner];
    if([self.adServer isEqualToString:@"MoPub"] && [self.adUnit isEqualToString:@"Interstitial"])
    [self loadMoPubInterstitial];
    
    
    // Do any additional setup after loading the view, typically from a nib.
}
    
-(void) viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    [self.bannerUnit stopAutoRefresh];
}
    
    
    
-(void) loadDFPBanner {
    
    self.bannerUnit = [[GamBannerAdUnit alloc] initWithConfigId:@"fb5fac4a-1910-4d3e-8a93-7bdbf6144312" size:CGSizeMake(300, 250) viewController:self adContainer:self.bannerView];
    [self.bannerUnit addAdditionalSizeWithSizes: @[[NSValue valueWithCGSize:CGSizeMake(320, 50)]]];
    [self.bannerUnit setAutoRefreshMillisWithTime:35000];
    self.bannerUnit.adSizeDelegate = self;
    self.dfpView = [[GAMBannerView alloc] initWithAdSize:kGADAdSizeMediumRectangle];
    self.dfpView.rootViewController = self;
    self.dfpView.adUnitID = @"/21807464892/pb_admax_300x250_top";
    self.dfpView.delegate = self;
    self.dfpView.appEventDelegate = self;
    [self.bannerView addSubview:self.dfpView];
    self.request = [[GAMRequest alloc] init];
    
    __weak ViewController *weakSelf = self;
    [self.bannerUnit fetchDemandWithAdObject:self.request completion:^(enum ResultCode result) {
        NSLog(@"Prebid demand result %ld", (long)result);
        dispatch_async(dispatch_get_main_queue(), ^{
            [weakSelf.dfpView loadRequest:weakSelf.request];
        });
    }];
}
    
-(void) loadDFPInterstitial {
    
    self.interstitialUnit = [[GamInterstitialAdUnit alloc] initWithConfigId:@"5ba30daf-85c5-471c-93b5-5637f3035149" viewController:self];
    self.request = [[GAMRequest alloc] init];
    __weak ViewController *weakSelf = self;
//    [self.interstitialUnit fetchDemandWithAdObject:self.request completion:^(enum ResultCode result) {
//        NSLog(@"Prebid demand result %ld", (long)result);
//        [weakSelf.dfpInterstitial loadRequest:weakSelf.request];
//    }];
    [self.interstitialUnit fetchDemandWithAdObject:self.request completion:^(enum ResultCode result) {
        NSLog(@"Prebid demand result %ld", (long)result);
        [GAMInterstitialAd loadWithAdManagerAdUnitID:@"/21807464892/pb_admax_interstitial" request:weakSelf.request completionHandler:^(GAMInterstitialAd * _Nullable interstitialAd, NSError * _Nullable error) {
            if (error) {
                NSLog(@"Failed to load interstitial ad with error: %@", [error localizedDescription]);
            } else {
                weakSelf.dfpInterstitial = interstitialAd;
                weakSelf.dfpInterstitial.appEventDelegate = self;
                [[Utils shared] findPrebidCreativeBidder:interstitialAd success:^(NSString* bidder) {
                    NSLog(@"bidder: %@", bidder);
                } failure:^(NSError * _Nonnull error) {
                    NSLog(@"error: %@", error.localizedDescription);
                    [weakSelf.dfpInterstitial presentFromRootViewController:weakSelf];
                }];
                [self.interstitialUnit isAdmaxAd:interstitialAd success:^(NSString* bidder) {
                    NSLog(@"ADMAX AD!!");
                } failure:^(void) {
                    NSLog(@"GOOGLE AD!!");
                }];
            }
        }];
    }];
}
    
-(void) loadMoPubBanner {
    
    MPMoPubConfiguration *configuration = [[MPMoPubConfiguration alloc] initWithAdUnitIdForAppInitialization:@"a935eac11acd416f92640411234fbba6"];
    
    [[MoPub sharedInstance] initializeSdkWithConfiguration:configuration completion:^{
        
    }];
    self.mopubAdView = [[MPAdView alloc] initWithAdUnitId:@"a935eac11acd416f92640411234fbba6"];
    self.mopubAdView.delegate = self;
    
    [self.bannerView addSubview:self.mopubAdView];
    
    self.bannerUnit = [[GamBannerAdUnit alloc] initWithConfigId:@"6ace8c7d-88c0-4623-8117-75bc3f0a2e45" size:CGSizeMake(300, 250) viewController:self adContainer:self.bannerView];
    // Do any additional setup after loading the view, typically from a nib.
    
    __weak ViewController *weakSelf = self;
    [self.bannerUnit fetchDemandWithAdObject:self.mopubAdView completion:^(enum ResultCode result) {         
        NSLog(@"Prebid demand result %ld", (long)result);
        [weakSelf.mopubAdView loadAd];
    }];
}
    
-(void) loadMoPubInterstitial {
    
    self.interstitialUnit = [[GamInterstitialAdUnit alloc] initWithConfigId:@"625c6125-f19e-4d5b-95c5-55501526b2a4" viewController:self];
    MPMoPubConfiguration *configuration = [[MPMoPubConfiguration alloc] initWithAdUnitIdForAppInitialization:@"2829868d308643edbec0795977f17437"];
    [[MoPub sharedInstance] initializeSdkWithConfiguration:configuration completion:nil];
    self.mopubInterstitial = [MPInterstitialAdController interstitialAdControllerForAdUnitId:@"2829868d308643edbec0795977f17437"];
    self.mopubInterstitial.delegate = self;
    __weak ViewController *weakSelf = self;
    [self.interstitialUnit fetchDemandWithAdObject:self.mopubInterstitial completion:^(enum ResultCode result) {
        NSLog(@"Prebid demand result %ld", (long)result);
        [weakSelf.mopubInterstitial loadAd];
    }];
    
    
}

- (void)onAdLoadedWithAdUnit:(AdUnit *)adUnit size:(CGSize)size adContainer:(UIView *)adContainer {
    NSLog(@"ADMAX onAdLoaded with Size: %fx%f", size.width, size.height);
}
    
#pragma mark :- DFP delegates
-(void) bannerViewDidReceiveAd:(GADBannerView *)bannerView {
    NSLog(@"Ad received");
    [[Utils shared] findPrebidCreativeSize:bannerView
                                   success:^(CGSize size) {
                                       if ([bannerView isKindOfClass:[GAMBannerView class]]) {
                                           GAMBannerView *dfpBannerView = (GAMBannerView *)bannerView;
                                           [dfpBannerView resize:GADAdSizeFromCGSize(size)];
                                       }
                                   } failure:^(NSError * _Nonnull error) {
                                       NSLog(@"error: %@", error.localizedDescription);
                                   }];

    [self.bannerUnit isAdmaxAd:bannerView success:^(NSString* bidder) {
        NSLog(@"ADMAX AD!!");
    } failure:^(void) {
        NSLog(@"GOOGLE AD!!");
    }];
}
    
- (void)bannerView:(GADBannerView *)bannerView didFailToReceiveAdWithError:(NSError *)error
{
    NSLog(@"adView:didFailToReceiveAdWithError: %@", error.localizedDescription);
}

#pragma mark :- Mopub delegates
-(void) adViewDidLoadAd:(MPAdView *)view adSize:(CGSize)adSize {
    NSLog(@"Ad received");
}

- (UIViewController *)viewControllerForPresentingModalView {
    return self;
}

- (void)interstitialDidLoadAd:(MPInterstitialAdController *)interstitial
{
    NSLog(@"Ad ready");
    if (self.mopubInterstitial.ready) {
        [self.mopubInterstitial showFromViewController:self];
    }
}
- (void)interstitialDidFailToLoadAd:(MPInterstitialAdController *)interstitial
{
    NSLog(@"Ad not ready");
}

-(void) adView:(GADBannerView *)banner didReceiveAppEvent:(NSString *)name withInfo:(NSString *)info
{
    self.bannerUnit.isGoogleAdServerAd = false;
    if (![self.bannerUnit isAdServerSdkRendering]) {
        [self.bannerUnit loadAd];
    }
}

-(void) interstitialAd:(GADInterstitialAd *)interstitial didReceiveAppEvent:(NSString *)name withInfo:(NSString *)info
{
    self.interstitialUnit.isGoogleAdServerAd = false;
    if (![self.interstitialUnit isAdServerSdkRendering]) {
        [self.interstitialUnit loadAd];
    } else {
        [self.dfpInterstitial presentFromRootViewController:self];
    }
}

@end
