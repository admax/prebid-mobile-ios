//
//  DemoAdmaxExceptionLogger.h
//  PrebidDemo
//
//  Created by Gwen on 06/07/2019.
//  Copyright © 2019 Prebid. All rights reserved.
//

@import AdmaxPrebidMobile;

@interface DemoAdmaxExceptionLogger : NSObject <AdmaxExceptionLogger>

@end
