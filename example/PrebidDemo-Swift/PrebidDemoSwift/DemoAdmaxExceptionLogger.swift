//
//  DemoAdmaxExceptionLogger.swift
//  PrebidDemo
//
//  Created by Gwen on 05/06/2019.
//  Copyright © 2019 Prebid. All rights reserved.
//

import Foundation
import AdmaxPrebidMobile

class DemoAdmaxExceptionLogger: NSObject, AdmaxExceptionLogger {
    func logException(cause: Error) {
        print(cause.localizedDescription)
    }
}
