/*   Copyright 2018-2019 Prebid.org, Inc.

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 */

import UIKit

import AdmaxPrebidMobile

import GoogleMobileAds

import SASDisplayKit

class BannerController: UIViewController, GADBannerViewDelegate, GADAppEventDelegate, SASBannerViewDelegate, AdSizeDelegate {

   @IBOutlet var appBannerView: UIView!

    @IBOutlet var adServerLabel: UILabel!

    var adServerName: String = ""
    
    var bidderName: String = ""

    let request = GAMRequest()
    
    var sasBanner: SASBannerView!
    
    var dfpBanner: GAMBannerView!

    var bannerUnit: BannerAdUnit!

    override func viewDidLoad() {
        super.viewDidLoad()

        adServerLabel.text = adServerName

//        bannerUnit = BannerAdUnit(configId: "fb5fac4a-1910-4d3e-8a93-7bdbf6144312", size: CGSize(width: 320, height: 50), viewController: self, adContainer: appBannerView)
        if (bidderName == "Xandr") {
            bannerUnit = BannerAdUnit(configId: "dbe12cc3-b986-4b92-8ddb-221b0eb302ef", size: CGSize(width: 320, height: 50), viewController: self, adContainer: appBannerView)
        } else if (bidderName == "Smart") {
            bannerUnit = BannerAdUnit(configId: "fe7d0514-530c-4fb3-9a52-c91e7c426ba6", size: CGSize(width: 320, height: 50), viewController: self, adContainer: appBannerView)
        }
//        bannerUnit.setAutoRefreshMillis(time: 35000)
        bannerUnit.adSizeDelegate = self
        //bannerUnit.addAdditionalSize(sizes: [CGSize(width: 300, height: 600)])

        if (adServerName == "DFP") {
            print("entered \(adServerName) loop" )
            loadDFPBanner(bannerUnit: bannerUnit)

        } else if (adServerName == "Smart") {
            print("entered \(adServerName) loop")
            loadSmartBanner(bannerUnit: bannerUnit)
        }
    }

    override func viewDidDisappear(_ animated: Bool) {
        // important to remove the time instance
        bannerUnit?.stopAutoRefresh()
    }
    
    func adView(_ banner: GADBannerView, didReceiveAppEvent name: String, withInfo info: String?) {
        print("GAD adView didReceiveAppEvent")
        if (AnalyticsEventType.bidWon.name() == name) {
            bannerUnit.isGoogleAdServerAd = false
            if !bannerUnit.isAdServerSdkRendering() {
                bannerUnit.loadAd()
            }
        }
    }
    
    func onAdLoaded(adUnit: AdUnit, size: CGSize, adContainer: UIView) {
        print("ADMAX onAdLoaded with Size: \(size)")
    }

    func loadDFPBanner(bannerUnit: AdUnit) {
        print("Google Mobile Ads SDK version: \(GADMobileAds.sharedInstance().versionNumber)")
        dfpBanner = GAMBannerView(adSize: GADAdSizeBanner)
//        dfpBanner.adUnitID = "/21807464892/pb_admax_300x250_top"
        dfpBanner.adUnitID = "/21807464892/pb_admax_320x50_top"
        dfpBanner.rootViewController = self
        dfpBanner.delegate = self
        dfpBanner.appEventDelegate = self
//        dfpBanner.backgroundColor = .red
        appBannerView.addSubview(dfpBanner)
//        request.testDevices = [ kGADSimulatorID, "2de8cd2491690938185052d38337abcf" ]

        bannerUnit.fetchDemand(adObject: self.request) { [weak self] (resultCode: ResultCode) in
            print("Prebid demand fetch for DFP \(resultCode.name())")
//            if let customTargeting = self?.request.customTargeting {
//                let targeting = NSMutableDictionary.init(dictionary: customTargeting)
//                targeting.setValue("10.00", forKey: "hb_pb")
//                self?.request.customTargeting = (targeting as! [AnyHashable : Any])
                print("DFP adrequest targeting: \(String(describing: self?.request.customTargeting))")
//            }
            self?.dfpBanner!.load(self?.request)
        }
    }
    
    func loadSmartBanner(bannerUnit: AdUnit) {
//        let sasAdPlacement: SASAdPlacement = SASAdPlacement(siteId: 104808, pageId: 936820, formatId: 15140)
        let sasAdPlacement: SASAdPlacement = SASAdPlacement(siteId: 285328, pageId: 1045725, formatId: 75125)
        self.sasBanner = SASBannerView(frame: CGRect(x: 0, y: 0, width: appBannerView.frame.width, height: 50))
        self.sasBanner.autoresizingMask = UIView.AutoresizingMask.flexibleWidth
        self.sasBanner.delegate = self
        self.sasBanner.modalParentViewController = self
        appBannerView.addSubview(sasBanner)
        
        let admaxBidderAdapter = SASAdmaxBidderAdapter(adUnit: bannerUnit)
        bannerUnit.fetchDemand(adObject: admaxBidderAdapter) { [weak self] (resultCode: ResultCode) in
            print("Prebid demand fetch for Smart \(resultCode.name())")
            self?.sasBanner!.load(with: sasAdPlacement, bidderAdapter: admaxBidderAdapter)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func bannerViewDidReceiveAd(_ bannerView: GADBannerView) {
        print("GAD bannerViewDidReceiveAd")
        Utils.shared.findPrebidCreativeSize(bannerView,
                                            success: { (size) in
                                                guard let bannerView = bannerView as? GAMBannerView else {
                                                    return
                                                }
                                                bannerView.resize(GADAdSizeFromCGSize(size))},
                                            failure: { (error) in
                                                print("error: \(error.localizedDescription)");
        })
    }

    func bannerView(_ bannerView: GADBannerView, didFailToReceiveAdWithError error: Error) {
        print("bannerView:didFailToReceiveAdWithError: \(error.localizedDescription)")
    }

    /// Tells the delegate an ad request failed.

    func viewControllerForPresentingModalView() -> UIViewController! {
        return self
    }
    
    func bannerViewDidLoad(_ bannerView: SASBannerView) {
        print("SAS bannerViewDidLoad")
    }
    
    func bannerView(_ bannerView: SASBannerView, didFailToLoadWithError error: Error) {
        print("SAS bannerView:didFailToLoadWithError: \(error.localizedDescription)")
    }

}
