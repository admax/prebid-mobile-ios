def podspec_template(version) 
  podspec = <<-EOS
Pod::Spec.new do |s|
  s.name         = 'AdmaxPrebidMobile'
  s.version      = '#{version}'
  s.summary      = 'AdmaxPrebidMobile is a lightweight framework that integrates directly with Prebid Server.'
  s.description  = <<-DESC
  Prebid-Mobile-SDK is a lightweight framework that integrates directly with Prebid Server to increase yield for publishers by adding more mobile buyers.'
  DESC
  s.homepage     = 'https://www.admaxmedia.io'
  s.license      = { :type => 'Apache License, Version 2.0', :text => <<-LICENSE
      Copyright 2018-2019 Admax SAS, Inc.
  
      Licensed under the Apache License, Version 2.0 (the 'License');
      you may not use this file except in compliance with the License.
      You may obtain a copy of the License at
  
      http://www.apache.org/licenses/LICENSE-2.0
  
      Unless required by applicable law or agreed to in writing, software
      distributed under the License is distributed on an 'AS IS' BASIS,
      WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
      See the License for the specific language governing permissions and
      limitations under the License.
      LICENSE
      }
  s.author             = { 'Admax SAS, Inc.' => 'contact@admaxmedia.io' }
  s.platform     = :ios, '9.0'
  s.swift_version = '5.0'
  s.source       = { :git => 'https://gitlab.com/admax/prebid-mobile-ios.git', :tag => '#{version}' }
  s.framework  = ['CoreTelephony', 'SystemConfiguration', 'UIKit', 'Foundation']
  s.static_framework = true
  s.default_subspec = 'Core'
  
  s.subspec 'Core' do |ss| 
    ss.source_files = 'AdmaxPrebidMobile/Classes/Core/**/*'
  end
    
  s.subspec 'GoogleMobileAds' do |ss|
    ss.dependency 'AdmaxPrebidMobile/Core'
    ss.source_files = 'AdmaxPrebidMobile/Classes/GoogleMobileAds/**/*'
    ss.dependency 'Google-Mobile-Ads-SDK', '~> 7.43'
    end
  end
  EOS
end
