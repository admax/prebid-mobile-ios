[![Build Status](https://api.travis-ci.org/prebid/prebid-mobile-ios.svg?branch=master)](https://travis-ci.org/prebid/prebid-mobile-ios)

# Prebid Mobile iOS SDK

Get started with Prebid Mobile by creating a Prebid Server account [here](http://prebid.org/prebid-mobile/prebid-mobile-pbs.html)

## Use Cocoapods?

Easily include the Prebid Mobile SDK for your priamy ad server in your Podfile/

```
platform :ios, '12.0'

target 'MyAmazingApp' do 
    pod 'AdmaxPrebidMobile'
    # or
    pod 'AdmaxPrebidMobile/GoogleMobileAds'
end
```

## Fastlane

Before all install bundler and run 

```sh
bundle install
```

### Make cocoapods specification 

```sh
bundle exec fastlane makePodspec version:2.1.1
```

### Push new version of sdk 

```sh
bundle exec fastlane publishPod version:2.1.1
```


### Try to build example project 

```sh 
bundle exec fastlane buildExample
```
