/*   Copyright 2018-2019 Prebid.org, Inc.

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 */

import Foundation
import CoreLocation

@objc public enum Gender: Int {
    case unknown
    case male
    case female
}

@objcMembers public class Targeting: NSObject {

    private var yearofbirth: Int = 0

    /**
     * This property gets the gender enum passed set by the developer
     */

    public var gender: Gender

    public var yearOfBirth: Int {
        return yearofbirth
    }

    /**
     * This property gets the year of birth value set by the application developer
     */
    public func setYearOfBirth(yob: Int) throws {
        let date = Date()
        let calendar = Calendar.current

        let year = calendar.component(.year, from: date)

        if (yob <= 1900 || yob >= year) {
            throw ErrorCode.yearOfBirthInvalid
        } else {
            yearofbirth = yob
        }
    }

    /**
     * This property clears year of birth value set by the application developer
     */
    public func clearYearOfBirth() {
            yearofbirth = 0
    }

    /**
     * The itunes app id for targeting
     */
    public var itunesID: String?

    /**
     * The application location for targeting
     */
    public var location: CLLocation?

    /**
     * The application location precision for targeting
     */
    public var locationPrecision: Int?

//    /**
//     * The boolean value set by the user to collect user data
//     */
//    public var subjectToGDPR: Bool {
//        set {
//            UserDefaults.standard.set(newValue, forKey: .PB_GDPR_SubjectToConsent)
//        }
//
//        get {
//            Log.info("subjectToGDPR")
//            var gdprConsent: Bool = false
//
//            if ((UserDefaults.standard.object(forKey: .PB_GDPR_SubjectToConsent)) != nil) {
//                Log.info("PB_GDPR_SubjectToConsent")
//                gdprConsent = UserDefaults.standard.bool(forKey: .PB_GDPR_SubjectToConsent)
//            } else if ((UserDefaults.standard.object(forKey: .IAB_GDPR_SubjectToConsent)) != nil) {
//                Log.info("IAB_GDPR_SubjectToConsent")
//                let stringValue: String = UserDefaults.standard.object(forKey: .IAB_GDPR_SubjectToConsent) as! String
//                Log.info("gdprConsent = \(stringValue)")
//                if (stringValue == "1") {
//                    gdprConsent = true
//                } else if (stringValue == "0") {
//                    gdprConsent = false
//                } else {
//                    Log.error("IAB_GDPR_SubjectToConsent value: \(stringValue) not conform to IAB standards")
//                }
//            }
//            return gdprConsent
//        }
//    }
//
//    /**
//     * The consent string for sending the GDPR consent
//     */
//    public var gdprConsentString: String? {
//        set {
//            UserDefaults.standard.set(newValue, forKey: .PB_GDPR_ConsentString)
//        }
//
//        get {
//            let pbString: String? = UserDefaults.standard.object(forKey: .PB_GDPR_ConsentString) as? String
//            let iabString: String? = UserDefaults.standard.object(forKey: .IAB_GDPR_ConsentString) as? String
//
//            var savedConsent: String?
//            if (pbString != nil) {
//                savedConsent = pbString
//            } else if (iabString != nil) {
//                savedConsent = iabString
//            }
//            return savedConsent
//        }
//    }
    
    // MARK: - COPPA
    /**
     * The boolean value set by the user to collect user data
     */
    public var subjectToCOPPA: Bool {
        set {
            StorageUtils.setPbCoppa(value: newValue)
        }
        
        get {
            return StorageUtils.pbCoppa()
        }
    }
    
    // MARK: - GDPR Subject
    /**
     * The boolean value set by the user to collect user data
     */
    public var subjectToGDPR: Bool? {
        set {
            StorageUtils.setPbGdprSubject(value: newValue)
        }

        get {
            var gdprSubject: Bool?

            if let pbGdpr = StorageUtils.pbGdprSubject() {
                gdprSubject = pbGdpr
            } else if let iabGdpr = StorageUtils.iabGdprSubject() {
                gdprSubject = iabGdpr
            }
            
            return gdprSubject
        }
    }

    // MARK: - GDPR Consent
    /**
     * The consent string for sending the GDPR consent
     */
    public var gdprConsentString: String? {
        set {
            StorageUtils.setPbGdprConsent(value: newValue)
        }

        get {
            var savedConsent: String?
            
            if let pbString = StorageUtils.pbGdprConsent() {
                savedConsent = pbString
            } else if let iabString = StorageUtils.iabGdprConsent() {
                savedConsent = iabString
            }
            
            return savedConsent
        }
    }
    
    // MARK: - TCFv2
    public var purposeConsents: String? {
        set {
            StorageUtils.setPbPurposeConsents(value: newValue)
        }

        get {
            var savedPurposeConsents: String?

            if let pbString = StorageUtils.pbPurposeConsents() {
                savedPurposeConsents = pbString
            } else if let iabString = StorageUtils.iabPurposeConsents() {
                savedPurposeConsents = iabString
            }

            return savedPurposeConsents

        }
    }

    /*
     Purpose 1 - Store and/or access information on a device
     */
    public func getDeviceAccessConsent() -> Bool? {
        let deviceAccessConsentIndex = 0
        return getPurposeConsent(index: deviceAccessConsentIndex)
    }

    func getPurposeConsent(index: Int) -> Bool? {

        var purposeConsent: Bool? = nil
        if let savedPurposeConsents = purposeConsents {
            let char = savedPurposeConsents[savedPurposeConsents.index(savedPurposeConsents.startIndex, offsetBy: index)]

            if char == "1" {
                purposeConsent = true
            } else if char == "0" {
                purposeConsent = false
            } else {
                Log.warn("invalid char:\(char)")
            }
        }

        return purposeConsent
    }

    
    public var storeURL: String?
    public var domain: String?

    /**
     * The class is created as a singleton object & used
     */
    public static let shared = Targeting()

    /**
     * The initializer that needs to be created only once
     */
    private override init() {
        gender = Gender.unknown
    }

}
