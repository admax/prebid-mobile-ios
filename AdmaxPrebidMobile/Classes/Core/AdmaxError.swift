//
//  AdmaxError.swift
//  PrebidMobile
//
//  Created by Gwen on 05/06/2019.
//  Copyright © 2019 AppNexus. All rights reserved.
//

import Foundation

enum AdmaxError: LocalizedError {
    case cause(message: String)
    
    var errorDescription: String? {
        switch self {
        case let .cause(message):
            return message
        }
    }
}
