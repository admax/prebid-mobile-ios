//
//  AnalyticsEventType.swift
//  PrebidMobile
//
//  Created by Gwen on 06/06/2019.
//  Copyright © 2019 AppNexus. All rights reserved.
//

import Foundation

@objc public enum AnalyticsEventType: Int {
    case bidWon
    case bidResponse
    
    public func name () -> String {
        switch self {
        case .bidWon: return "bidWon"
        case .bidResponse: return "bidResponse"
        }
    }
}
