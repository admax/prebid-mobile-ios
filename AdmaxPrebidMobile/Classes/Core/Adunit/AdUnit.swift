/*   Copyright 2018-2019 Prebid.org, Inc.
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 http://www.apache.org/licenses/LICENSE-2.0
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 */

import Foundation
import ObjectiveC.runtime

@objcMembers public class AdUnit: NSObject, DispatcherDelegate, DemandBiddingManagerListener {
    

    public var prebidConfigId: String! = ""

    var adSizes = Array<CGSize> ()
    
    private weak var viewController: UIViewController?
    public var rootViewController: UIViewController? {
        get {
           return viewController
        }

        set {
            viewController = newValue
        }
    }

    var identifier: String

    var dispatcher: Dispatcher?
    
    var bidManager: BidManager?

    private var customKeywords = [String: Array<String>]()

    //This flag is set to check if the refresh needs to be made though the user has not invoked the fetch demand after initialization
    private var isInitialFetchDemandCallMade: Bool = false

    private var adServerObject: AnyObject?

    private var closure: (ResultCode) -> Void

    //notification flag set to check if the prebid response is received within the specified time
    var didReceiveResponse: Bool! = false

    //notification flag set to determine if delegate call needs to be made after timeout delegate is sent
    var timeOutSignalSent: Bool! = false
    
    var biddingManagers: [DemandBiddingManager] = []
    private var winningBiddingManager: DemandBiddingManager?
    private var winningBid: Double = 0
    private var winningDemand: [String:String]?
    private var winningAdm: String = ""
    var biddersCount: Int = 0
    private var hasAuctionTimedOut: Bool = false
    private var isAuctionRunning: Bool = false
    public var adContainer: UIView?
    public weak var adSizeDelegate: AdSizeDelegate?
    private var isSmartAdServerAdInternal: Bool = true
    public var isSmartAdServerAd: Bool {
        get {
            return isSmartAdServerAdInternal
        }
        set {
            isSmartAdServerAdInternal = newValue
            if (isSmartAdServerSdkRendering()) {
                callAdSizeListenerDelegate()
            }
        }
    }
    private var isGoogleAdServerAdInternal: Bool = true
    public var isGoogleAdServerAd: Bool {
        get {
            return isGoogleAdServerAdInternal
        }
        set {
            isGoogleAdServerAdInternal = newValue
            if (isAdServerSdkRendering()) {
                callAdSizeListenerDelegate()
            }
        }
    }
    private var adType: AdType

    init(configId: String, size: CGSize?, viewController: UIViewController?, adContainer: UIView? = nil) {
        self.adContainer = adContainer
        self.closure = {_ in return}
        prebidConfigId = configId
        if let givenSize = size {
            adSizes.append(givenSize)
        }
        identifier = UUID.init().uuidString
        self.viewController = viewController
        self.adType = .BANNER
        super.init()
    }
    
    init(configId: String, size: CGSize?, viewController: UIViewController?, adContainer: UIView? = nil, adType: AdType) {
        self.adContainer = adContainer
        self.closure = {_ in return}
        prebidConfigId = configId
        if let givenSize = size {
            adSizes.append(givenSize)
        }
        identifier = UUID.init().uuidString
        self.viewController = viewController
        self.adType = adType
        super.init()
    }

    dynamic public func fetchDemand(adObject: AnyObject, completion: @escaping(_ result: ResultCode) -> Void) {

        Log.info("FetchingDemand")
        isSmartAdServerAdInternal = true
        isGoogleAdServerAdInternal = true

        if !(self is NativeRequest) {
            for size in adSizes {
                if (size.width < 0 || size.height < 0) {
                    completion(ResultCode.prebidInvalidSize)
                    return
                }
            }
        }
        
        Utils.shared.removeHBKeywords(adObject: adObject)

        if (prebidConfigId.isEmpty || (prebidConfigId.trimmingCharacters(in: CharacterSet.whitespaces)).count == 0) {
            completion(ResultCode.prebidInvalidConfigId)
            return
        }
        if (Prebid.shared.prebidServerAccountId.isEmpty || (Prebid.shared.prebidServerAccountId.trimmingCharacters(in: CharacterSet.whitespaces)).count == 0) {
            completion(ResultCode.prebidInvalidAccountId)
            return
        }

        if !isInitialFetchDemandCallMade {
            isInitialFetchDemandCallMade = true
            startDispatcher()
        }

        biddersCount = biddingManagers.count + 1
        hasAuctionTimedOut = false
        isAuctionRunning = true
        DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(Prebid.shared.clientAuctionTimeoutMs), execute: {
            if self.isAuctionRunning {
                self.hasAuctionTimedOut = true
                self.declareWinningBid()
                Log.debug("Auction Timed Out!")
            }
        })
        
        didReceiveResponse = false
        timeOutSignalSent = false
        self.closure = completion
        adServerObject = adObject
        
        for biddingManager in biddingManagers {
            biddingManager.loadBid(listener: self)
        }
        
        bidManager = BidManager(adUnit: self)

        bidManager!.requestBidsForAdUnit { (bidResponse, resultCode) in
            self.didReceiveResponse = true
            if (bidResponse != nil) {
                if (!self.timeOutSignalSent) {
//                    Utils.shared.validateAndAttachKeywords (adObject: adObject, bidResponse: bidResponse!)
//                    completion(resultCode)
                    let admaxBiddingAdResponse = AdmaxBiddingAdResponse(bidResponse: bidResponse!)
                    self.onDemandBiddingManagerAdLoaded(adResponse: admaxBiddingAdResponse)
                }

            } else {
                if (!self.timeOutSignalSent) {
                    Log.debug("Prebid resultCode: \(resultCode)")
                    self.onDemandBiddingManagerAdFailedToLoad(e: AdmaxError.cause(message: resultCode.name()))
//                    completion(resultCode)
                }
            }
        }

        DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(Prebid.shared.admaxAuctionTimeoutMs), execute: {
            if (!self.didReceiveResponse) {
                self.timeOutSignalSent = true
//                completion(ResultCode.prebidDemandTimedOut)
                self.onDemandBiddingManagerAdFailedToLoad(e: AdmaxError.cause(message: ResultCode.prebidDemandTimedOut.name()))
            }
        })
    }

    var userKeywords: [String: [String]] {
        Log.info("user keywords are \(customKeywords)")
        return customKeywords
    }
    
    public func loadAd() {
        winningBiddingManager?.loadAd()
    }
    
    public func isSmartAdServerSdkRendering() -> Bool {
        return (winningBiddingManager == nil && winningAdm.isEmpty) || isSmartAdServerAd
    }
    
    public func isAdServerSdkRendering() -> Bool {
        return (winningBiddingManager == nil && winningAdm.isEmpty) || isGoogleAdServerAd
    }

    /**
     * This method obtains the user keyword & value user for targeting
     * if the key already exists the value will be appended to the list. No duplicates will be added
     */
    public func addUserKeyword(key: String, value: String) {
        var existingValues: [String] = []
        if (customKeywords[key] != nil) {
            existingValues = customKeywords[key]!
        }
        if (!existingValues.contains(value)) {
            existingValues.append(value)
            customKeywords[key] = existingValues
        }
    }

    /**
     * This method obtains the user keyword & values set for user targeting.
     * the values if the key already exist will be replaced with the new set of values
     */
    public func addUserKeywords(key: String, value: [String]) {

        customKeywords[key] = value

    }

    /**
     * This method allows to remove all the user keywords set for user targeting
     */
    public func clearUserKeywords() {

        if (customKeywords.count > 0 ) {
            customKeywords.removeAll()
        }

    }

    /**
     * This method allows to remove specific user keyword & value set from user targeting
     */
    public func removeUserKeyword(forKey: String) {
        if (customKeywords[forKey] != nil) {
            customKeywords.removeValue(forKey: forKey)
        }
    }

    /**
     * This method allows to set the auto refresh period for the demand
     *
     * - Parameter time: refresh time interval
     */
    public func setAutoRefreshMillis(time: Double) {

        stopDispatcher()

        guard time >= .PB_MIN_RefreshTime else {
            Log.error("auto refresh not set as the refresh time is less than to \(.PB_MIN_RefreshTime as Double) seconds")
            return
        }

        initDispatcher(refreshTime: time)

        if isInitialFetchDemandCallMade {
            startDispatcher()
        }
    }

    /**
     * This method stops the auto refresh of demand
     */
    public func stopAutoRefresh() {
        stopDispatcher()
    }
    
    public func startAutoRefresh() {
        startDispatcher()
    }

    func refreshDemand() {
        if (adServerObject != nil) {
            self.fetchDemand(adObject: adServerObject!, completion: self.closure)
        }

    }

    func initDispatcher(refreshTime: Double) {
        self.dispatcher = Dispatcher.init(withDelegate: self, autoRefreshMillies: refreshTime)
    }

    func startDispatcher() {
        guard let dispatcher = self.dispatcher else {
            Log.verbose("Dispatcher is nil")
            return
        }

        dispatcher.start()
    }

    func stopDispatcher() {
        guard let dispatcher = self.dispatcher else {
            Log.verbose("Dispatcher is nil")
            return
        }

        dispatcher.stop()
        self.dispatcher = nil
    }
    
    public func onDemandBiddingManagerAdLoaded(adResponse: DemandBiddingAdResponse) {
        Log.debug("onDemandBiddingManagerAdLoaded: \(adResponse.bidder)")
        biddersCount -= 1
        var winningBidder: String = ""
        if adResponse.price > winningBid {
            winningBid = adResponse.price
            winningBidder = adResponse.bidder
            winningDemand = adResponse.demand
            winningAdm = adResponse.adm
            Log.debug("New winningDemad: \(winningBidder)")
            if (winningBidder == .Admax_Bidder || winningBidder == .Facebook_Bidder) {
                winningBiddingManager = nil
            } else {
                for manager in biddingManagers {
                    if manager.bidder == winningBidder {
                        winningBiddingManager = manager
                        break
                    }
                }
            }
        }
        if (biddersCount == 0 && !hasAuctionTimedOut) {
            self.declareWinningBid()
        }
    }
    
    public func onDemandBiddingManagerAdFailedToLoad(e: Error) {
        Log.debug("onDemandBiddingManagerAdFailedToLoad with error: \(e.localizedDescription)")
        biddersCount -= 1
        if (biddersCount == 0 && !hasAuctionTimedOut) {
            self.declareWinningBid()
        }
    }
    
    public func onDemandBiddingAdSizeReady(size: CGSize) {
        if let adContainer = adContainer {
            adSizeDelegate?.onAdLoaded(adUnit: self, size: size, adContainer: adContainer)
        }
    }
    
    private func declareWinningBid() {
        cancelTimerAndBiddingManagers()
        if (winningDemand != nil) {
            if adServerObject != nil {
                Utils.shared.validateAndAttachKeywords (adObject: adServerObject!, customKeywords: winningDemand!)
                Log.debug("Successfully set keywords for winning demand: \(String(describing: winningDemand))")
            }
            self.closure(ResultCode.prebidDemandFetchSuccess)
        } else {
            self.closure(ResultCode.prebidDemandNoBids)
        }
    }
    
    private func cancelTimerAndBiddingManagers() {
        isAuctionRunning = false
        for biddingManager in biddingManagers {
            biddingManager.cancel()
        }
    }
    
    private func callAdSizeListenerDelegate() {
        if let adSizeDelegate = adSizeDelegate, let winningDemand = winningDemand {
            if let hbSize = winningDemand["hb_size"] {
                if let size = Utils.shared.stringToCGSize(hbSize), let adContainer = adContainer {
                    adSizeDelegate.onAdLoaded(adUnit: self, size: size, adContainer: adContainer)
                }
            }
        }
    }

}

class AdmaxBiddingAdResponse: DemandBiddingAdResponse {
    
    var bidResponse: BidResponse
    
    init(bidResponse: BidResponse) {
        self.bidResponse = bidResponse
    }
    
    var bidder: String {
        if (!bidResponse.fbAdm.isEmpty) {
            return .Facebook_Bidder
        }
        return .Admax_Bidder
    }
    
    var price: Double {
        get {
            let keyValuePrefix: String = AdmaxConfigUtil.getKeyvaluePrefix(admaxConfig: Prebid.shared.admaxConfig)
            if let hbPb = bidResponse.customKeywords[keyValuePrefix] {
                if let hbPb = Double(hbPb) {
                    return hbPb
                }
            }
            return 0
        }
    }
    
    var demand: [String : String] {
        get {
            return bidResponse.customKeywords
        }
    }
    
    var adm: String {
        get {
            return bidResponse.fbAdm
        }
    }
}
