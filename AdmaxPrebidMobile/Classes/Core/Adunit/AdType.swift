//
//  AdType.swift
//  AdmaxPrebidMobile
//
//  Created by Gwen on 16/11/2020.
//

import Foundation

enum AdType {
    case BANNER
    case INTERSTITIAL
    case NATIVE
}
