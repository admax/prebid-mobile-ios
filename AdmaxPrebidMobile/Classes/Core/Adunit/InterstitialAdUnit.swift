//
//  InterstitialAdUnit.swift
//  AdmaxPrebidMobile
//
//  Created by Gwen on 31/01/2020.
//

import Foundation

@objcMembers public class InterstitialAdUnit: BannerBaseAdUnit {
    
    public init(configId: String, viewController: UIViewController) {
        super.init(configId: configId, size: CGSize(width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height), viewController: viewController, adType: .INTERSTITIAL)
        if let admaxConfig = Prebid.shared.admaxConfig {
            if let smartBidderConfig = AdmaxConfigUtil.getBidderConfig(admaxConfig: admaxConfig, admaxConfigId: configId, bidder: .Smart_Bidder) {
                biddingManagers.append(SmartBiddingManager(adType: .Adtype_Interstitial, configId: configId, bidderConfig: smartBidderConfig, viewController: rootViewController))
            }
        }
    }
    
    public func isAdmaxAd(_ adObject: NSObject, success: @escaping (String) -> Void, failure: @escaping () -> Void) -> Void {
        Utils.shared.findPrebidCreativeBidder(adObject,
                                            success: {(bidder) in
                                                success(bidder)
        },
                                            failure: { (error) in
                                                failure()
        })
    }
}
