//
//  BannerAdUnit.swift
//  AdmaxPrebidMobile
//
//  Created by Gwen on 31/01/2020.
//

import Foundation

@objcMembers public class BannerAdUnit: BannerBaseAdUnit {

    public init(configId: String, size: CGSize, viewController: UIViewController?, adContainer: UIView?) {
        super.init(configId: configId, size: size, viewController: viewController, adContainer: adContainer, adType: .BANNER)
        if let admaxConfig = Prebid.shared.admaxConfig {
            if let smartBidderConfig = AdmaxConfigUtil.getBidderConfig(admaxConfig: admaxConfig, admaxConfigId: configId, bidder: .Smart_Bidder) {
                biddingManagers.append(SmartBiddingManager(adType: .Adtype_Banner, configId: configId, bidderConfig: smartBidderConfig, viewController: rootViewController, adContainer: adContainer))
            }
        }
    }
    
    public func isAdmaxAd(_ adView: UIView, success: @escaping (String) -> Void, failure: @escaping () -> Void) -> Void {
        Utils.shared.findPrebidCreativeBannerBidder(adView,
                                            success: {(bidder) in
                                                success(bidder)
        },
                                            failure: { (error) in
                                                failure()
        })
    }

     public func addAdditionalSize(sizes: [CGSize]) {
        adSizes += sizes
    }
}
