//
//  AdSizeDelegate.swift
//  AdmaxPrebidMobile
//
//  Created by Gwen on 02/04/2020.
//

import Foundation

@objc public protocol AdSizeDelegate {
    func onAdLoaded(adUnit: AdUnit, size: CGSize, adContainer: UIView)
}
