//
//  NativeEventTracker.swift
//  AdmaxPrebidMobile
//
//  Created by Gwen on 22/08/2021.
//

import UIKit

@objc public class NativeEventTracker: NSObject {
    
    var event: EventType
    var methods: Array<EventTracking>
    var ext: AnyObject?
    
    @objc
    public init(event: EventType, methods: Array<EventTracking>) {
        self.event = event
        self.methods = methods
    }
    
    func getEventTracker() -> [AnyHashable: Any] {
        var methodsList:[Int] = []
        
        for method:EventTracking in methods {
            methodsList.append(method.value)
        }
        
        let event = [
            "event": self.event.value,
            "methods": methodsList
            ] as [String : Any]
        
        return event
    }
}

public class EventType: SingleContainerInt {
    @objc
    public static let Impression = EventType(1)

    @objc
    public static let ViewableImpression50 = EventType(2)

    @objc
    public static let ViewableImpression100 = EventType(3)

    @objc
    public static let ViewableVideoImpression50 = EventType(4)

    @objc
    public static let Custom = EventType(500)
}


public class EventTracking: SingleContainerInt {
    @objc
    public static let Image = EventTracking(1)

    @objc
    public static let js = EventTracking(2)

    @objc
    public static let Custom = EventTracking(500)
}
