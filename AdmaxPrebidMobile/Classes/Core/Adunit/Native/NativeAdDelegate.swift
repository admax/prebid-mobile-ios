//
//  NativeAdDelegate.swift
//  AdmaxPrebidMobile
//
//  Created by Gwen on 22/08/2021.
//

import Foundation

@objc public protocol NativeAdDelegate : AnyObject{
    /**
     * A successful Prebid Native ad is returned
     *
     * @param ad use this instance for displaying
     */
    func nativeAdLoaded(ad:NativeAd)
    /**
     * Prebid Native was not found in the server returned response,
     * Please display the ad as regular ways
     */
    func nativeAdNotFound()
    /**
     * Prebid Native ad was returned, however, the bid is not valid for displaying
     * Should be treated as on ad load failed
     */
    func nativeAdNotValid()
}
