//
//  NativeAdEventDelegate.swift
//  AdmaxPrebidMobile
//
//  Created by Gwen on 22/08/2021.
//

import Foundation

@objc public protocol NativeAdEventDelegate : AnyObject {
    /**
     * Sent when the native ad is expired.
     */
    @objc optional func adDidExpire(ad:NativeAd)
    /**
     * Sent when the native view is clicked by the user.
     */
    @objc optional func adWasClicked(ad:NativeAd)
    /**
     * Sent when  an impression is recorded for an native ad
     */
    @objc optional func adDidLogImpression(ad:NativeAd)
}
