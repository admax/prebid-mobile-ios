//
//  TrackerInfo.swift
//  AdmaxPrebidMobile
//
//  Created by Gwen on 22/08/2021.
//

import UIKit

class TrackerInfo: NSObject {

    var URL : String?
    var dateCreated : Date?
    var expired = false
    var numberOfTimesFired = 0
    private var expirationTimer : Timer?
    private static let trackerExpirationInterval : TimeInterval = 3600
    
    init(URL: String) {
        self.URL = URL
        self.dateCreated = Date()
        super.init()
        createExpirationTimer()
    }
    func createExpirationTimer(){
        expirationTimer = Timer.scheduledTimer(withTimeInterval: TrackerInfo.trackerExpirationInterval, repeats: false, block: { [weak self] timer in
            timer.invalidate()
            guard let strongSelf = self else {
                Log.debug("FAILED TO ACQUIRE strongSelf for TrackerInfo")
                return
            }
            strongSelf.expired = true
        })
    }
    
    deinit {
        expirationTimer?.invalidate()
    }
}
