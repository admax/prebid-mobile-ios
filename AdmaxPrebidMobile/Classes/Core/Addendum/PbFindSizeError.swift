//
//  PbFindSizeError.swift
//  AdmaxPrebidMobile
//
//  Created by Gwen on 23/08/2021.
//

import Foundation

class PbFindSizeError: NSError {
    convenience init(code: Int, userInfo dict: [String : Any]? = nil) {
        self.init()
    }
    
}
