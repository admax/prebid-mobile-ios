//
//  DemandBiddingAdResponse.swift
//  AdmaxPrebidMobile
//
//  Created by Gwen on 25/03/2020.
//

import Foundation

@objc public protocol DemandBiddingAdResponse {
    var bidder: String { get }
    var price: Double { get }
    var demand: [String:String] { get }
    var adm: String { get }
}
