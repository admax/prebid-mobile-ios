//
//  SmartBiddingManager.swift
//  AdmaxPrebidMobile
//
//  Created by Gwen on 25/03/2020.
//

import Foundation
import SASDisplayKit

public class SmartBiddingManager: NSObject, DemandBiddingManager, SASBiddingManagerDelegate, SASInterstitialManagerDelegate, SASBannerViewDelegate {
    public var bidder: String = .Smart_Bidder
    var biddingManager: SASBiddingManager?
    var adType: String
    var adResponse: SASBiddingAdResponse?
    var bannerView: SASBannerView?
    var parallaxInfos: (CGFloat, CGFloat)?
    var interstitialManager: SASInterstitialManager?
    weak var listener: DemandBiddingManagerListener?
    var demandAdResponse: DemandBiddingAdResponse?
    var admaxConfigId: String
    weak var viewController: UIViewController?
    var adContainer: UIView?
    
    init(adType: String, configId: String, bidderConfig: [String:Any], viewController: UIViewController?, adContainer: UIView? = nil) {
        self.adContainer = adContainer
        self.adType = adType
        admaxConfigId = configId
        self.viewController = viewController
        super.init()
        if let adPlacement = getAdPlacement(bidderConfig: bidderConfig) {
            biddingManager = SASBiddingManager(adPlacement: adPlacement, biddingAdFormatType: adType == .Adtype_Banner ? SASBiddingAdFormatType.banner : SASBiddingAdFormatType.interstitial, currency: getCurrency(bidderConfig: bidderConfig), delegate: self)
        }
        self.parallaxInfos = getParallaxInfos(bidderConfig: bidderConfig)
    }
    
    func getAdPlacement(bidderConfig: [String:Any]) -> SASAdPlacement? {
        var siteId: Int
        var pageId: Int
        var formatId: Int
        var target: String
        if let smartSiteId = bidderConfig[.Smart_Site_Id] as? Int {
            siteId = smartSiteId
        } else {
            Log.debug("Missing siteId")
            return nil
        }
        if let smartPageIdString = bidderConfig[.Smart_Page_Id] as? String, let smartPageId = Int(smartPageIdString) {
            pageId = smartPageId
        } else {
            Log.debug("Missing pageId")
            return nil
        }
        if let smartFormatId = bidderConfig[.Smart_format_Id] as? Int {
            formatId = smartFormatId
        } else {
            Log.debug("Missing formatId")
            return nil
        }
        if let smartTarget = bidderConfig[.Smart_target] as? String {
            target = smartTarget
        } else {
            Log.debug("Missing target")
            return nil
        }
        if (!SASConfiguration.shared.isConfigured) {
            SASConfiguration.shared.configure(siteId: siteId)
        }
        return SASAdPlacement(siteId: siteId, pageId: pageId, formatId: formatId, keywordTargeting: target)
    }
    
    func getCurrency(bidderConfig: [String:Any]) -> String {
        if let smartCurrency = bidderConfig[.Smart_Currency] as? String {
            return smartCurrency
        } else {
            Log.debug("Missing currency, using default: \(String.Smart_Default_Currency)")
        }
        return .Smart_Default_Currency
    }
    
    func getParallaxInfos(bidderConfig: [String:Any]) -> (CGFloat, CGFloat) {
        var topMargin: CGFloat
        var height: CGFloat
        if let smartTopMargin = bidderConfig[.Smart_Top_Margin] as? CGFloat {
            topMargin = smartTopMargin
        } else {
            topMargin = -1
        }
        if let smartHeight = bidderConfig[.Smart_Height] as? CGFloat {
            height = smartHeight
        } else {
            height = -1
        }
        return (topMargin, height)
    }
    
    public func loadBid(listener: DemandBiddingManagerListener) {
        self.listener = listener
        biddingManager?.load()
    }
    
    public func cancel() {
        biddingManager?.delegate = nil
    }
    
    public func loadAd() {
        if (adType == .Adtype_Banner) {
            loadBannerAd()
        } else {
            loadInterstitialAd()
        }
    }
    
    private func loadBannerAd() {
        Log.debug("SMART loading banner ad")
        if let adResponse = adResponse, let adContainer = adContainer {
            bannerView = SASBannerView(frame: CGRect(x: 0, y: 0, width: adContainer.frame.width, height: adContainer.frame.height))
            if let parallaxInfos = self.parallaxInfos, parallaxInfos.0 != -1, parallaxInfos.1 != -1 {
                bannerView?.parallaxInfos = SASParallaxInfos.init(viewportTopOrigin: parallaxInfos.0, viewportHeight: parallaxInfos.1)
            }
            bannerView?.modalParentViewController = viewController
            for subview in adContainer.subviews {
                subview.removeFromSuperview()
            }
            bannerView?.delegate = self
            bannerView?.load(adResponse)
            adContainer.addSubview(bannerView!)
        }
        
    }
    
    private func loadInterstitialAd() {
        Log.debug("SMART loading interstitial ad")
        if (adResponse != nil) {
            interstitialManager?.load()
        }
    }
    
    public func biddingManager(_ biddingManager: SASBiddingManager, didLoad biddingAdResponse: SASBiddingAdResponse) {
        if listener != nil {
            if adType == .Adtype_Interstitial {
                interstitialManager = SASInterstitialManager(biddingAdResponse: biddingAdResponse, delegate: self)
            }
            adResponse = biddingAdResponse
            demandAdResponse = SmartDemandBiddingAdResponse(adResponse: biddingAdResponse)
            listener?.onDemandBiddingManagerAdLoaded(adResponse: demandAdResponse!)
        }
    }
    
    public func biddingManager(_ biddingManager: SASBiddingManager, didFailToLoadWithError error: Error) {
        if listener != nil {
            listener?.onDemandBiddingManagerAdFailedToLoad(e: error)
        }
    }
    
    public func bannerView(_ bannerView: SASBannerView, didDownloadAd ad: SASAd) {
        let size = ad.portraitSize
        let width = Int(size.width)
        let height = Int(size.height)
        let renderedSize = String(describing: width) + "x" + String(describing: height)
        Log.debug("SMART renderedSize = \(renderedSize)")
        listener?.onDemandBiddingAdSizeReady(size: CGSize(width: width, height: height))
        self.bannerView?.frame.size = size
    }
    
    public func bannerViewDidFail(toResize bannerView: SASBannerView, error: Error) {
        Log.debug("Smart banner view did fail with error \(error)")
    }
    
    public func bannerView(_ bannerView: SASBannerView, didFailToLoadWithError error: Error) {
        Log.debug("Smart banner view did fail to load with error \(error)")
    }
    
    public func interstitialManager(_ manager: SASInterstitialManager, didLoad ad: SASAd) {
        Log.debug("Interstitial loading completed")
        if let viewController = viewController {
            interstitialManager?.show(from: viewController)
        }
    }
    
    public func interstitialManager(_ manager: SASInterstitialManager, didFailToLoadWithError error: Error) {
        Log.debug("Intersitial did fail to load with error \(error)")
    }
    
    public func interstitialManager(_ manager: SASInterstitialManager, didFailToShowWithError error: Error) {
        Log.debug("Interstitial did fail to show with error \(error)")
    }
    
    public func interstitialManager(_ manager: SASInterstitialManager, didAppearFrom viewController: UIViewController) {
        Log.debug("Interstitital did appear")
    }
    
    public func interstitialManager(_ manager: SASInterstitialManager, didDisappearFrom viewController: UIViewController) {
        Log.debug("Interstitial did disappear")
    }
    
    public func interstitialManager(_ manager: SASInterstitialManager, willDismissModalViewFrom viewController: UIViewController) {
        Log.debug("Interstitial will dismiss modal view")
    }
    
    public func interstitialManager(_ manager: SASInterstitialManager, willPresentModalViewFrom viewController: UIViewController) {
        Log.debug("Interstitial will present modal view")
    }
}

class SmartDemandBiddingAdResponse: DemandBiddingAdResponse {
    var sasBiddingAdResponse: SASBiddingAdResponse
    
    init(adResponse: SASBiddingAdResponse) {
        sasBiddingAdResponse = adResponse
    }
    var bidder: String = .Smart_Bidder
    
    var price: Double {
        get {
            return sasBiddingAdResponse.biddingAdPrice.cpm
        }
    }
    
    var demand: [String : String] {
        get {
            let keyValuePrefix: String = AdmaxConfigUtil.getKeyvaluePrefix(admaxConfig: Prebid.shared.admaxConfig)
            let pgConfig: PriceGranularityConfig = AdmaxConfigUtil.getPriceGranularityConfig(admaxConfig: Prebid.shared.admaxConfig)
            
            var smartDemand: [String:String] = [:]
            smartDemand["hb_bidder"] = .Smart_Bidder
            smartDemand["hb_env"] = "mobile-app"
            smartDemand[keyValuePrefix] = pgConfig.getPriceBucket(cpm: sasBiddingAdResponse.biddingAdPrice.cpm)
            return smartDemand
        }
    }
    
    var adm: String {
        get {
            return ""
        }
    }
    
    
}
