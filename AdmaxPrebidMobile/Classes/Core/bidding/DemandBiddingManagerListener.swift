//
//  DemandBiddingManagerListener.swift
//  AdmaxPrebidMobile
//
//  Created by Gwen on 25/03/2020.
//

import Foundation

@objc public protocol DemandBiddingManagerListener {
    func onDemandBiddingManagerAdLoaded(adResponse: DemandBiddingAdResponse)
    func onDemandBiddingManagerAdFailedToLoad(e: Error)
    func onDemandBiddingAdSizeReady(size: CGSize)
}
