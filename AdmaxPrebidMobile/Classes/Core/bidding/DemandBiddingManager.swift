//
//  DemandBiddingManager.swift
//  AdmaxPrebidMobile
//
//  Created by Gwen on 25/03/2020.
//

import Foundation

@objc public protocol DemandBiddingManager {
    var bidder: String { get }
    func loadBid(listener: DemandBiddingManagerListener)
    func cancel()
    func loadAd()
}
