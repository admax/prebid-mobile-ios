/*   Copyright 2018-2019 Prebid.org, Inc.

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 */

import Foundation
import WebKit
import GoogleMobileAds

public class Utils: NSObject {

    /**
     * The class is created as a singleton object & used
     */
    @objc
    public static let shared = Utils()

    /**
     * The initializer that needs to be created only once
     */
    private override init() {
        super.init()
    }
    
    
    @objc
    public weak var delegate: NativeAdDelegate?

    private let DFP_BANNER_VIEW_CLASSNAME = "DFPBannerView"
    private let DFP_WEBADVIEW_CLASSNAME = "GADWebAdView"
    private let MOPUB_NATIVE_AD_CLASSNAME = "MPNativeAd"
    private let DFP_CUSTOM_TEMPLATE_AD_CLASSNAME = "GADNativeCustomTemplateAd"
    private let GAD_CUSTOM_NATIVE_AD = "GADCustomNativeAd"
    private let INNNER_HTML_SCRIPT = "document.body.innerHTML"

    @objc
    public func convertDictToMoPubKeywords(dict: Dictionary<String, String>) -> String {
        return dict.toString(entrySeparator: ",", keyValueSeparator: ":")
        
    }

    @objc public func removeHBKeywords (adObject: AnyObject) {
        Log.info("RemovingHBKeywords")
        let adServerObject: String = String(describing: type(of: adObject))
        if (adServerObject == .DFP_Object_Name || adServerObject == .DFP_O_Object_Name ||
            adServerObject == .DFP_N_Object_Name || adServerObject == .GAD_N_Object_Name ||
            adServerObject == .GAD_Object_Name || adServerObject == .GAM_Object_Name) {
            let hasDFPMember = adObject.responds(to: NSSelectorFromString("setCustomTargeting:"))
            if (hasDFPMember) {
                //check if the publisher has added any custom targeting. If so then merge the bid keywords to the same.
                if (adObject.value(forKey: "customTargeting") != nil) {
                    var existingDict: [String: Any] = adObject.value(forKey: "customTargeting") as! [String: Any]
                    for (key, _)in existingDict {
                        if (key.starts(with: "hb_")) {
                            existingDict[key] = nil
                        }
                    }
                    adObject.setValue( existingDict, forKey: "customTargeting")
                }
            }
        }

        if (adServerObject == .MoPub_Object_Name || adServerObject == .MoPub_Interstitial_Name) {
            let hasMoPubMember = adObject.responds(to: NSSelectorFromString("setKeywords:"))

            if (hasMoPubMember) {
                //for mopub the keywords has to be set as a string seperated by ,
                // split the dictionary & construct a string comma separated
                if (adObject.value(forKey: "keywords") != nil) {
                    let targetingKeywordsString: String = adObject.value(forKey: "keywords") as! String

                    let commaString: String = ","
                    if (targetingKeywordsString != "") {
                        let keywordsArray = targetingKeywordsString.components(separatedBy: ",")
                        var i = 0
                        var newString: String = ""
                        while i < keywordsArray.count {
                            if (!keywordsArray[i].starts(with: "hb_")) {

                                if ( newString == .EMPTY_String) {
                                    newString = keywordsArray[i]
                                } else {
                                    newString += commaString + keywordsArray[i]
                                }
                            }

                            i += 1
                        }

                        Log.info("MoPub targeting keys are \(newString)")
                        adObject.setValue( newString, forKey: "keywords")


                    }
                }

            }

        }
        Log.info("HBKeywords removed")
    }

    @objc public func findPrebidCreativeBidder(_ adObject: NSObject, success: @escaping (String) -> Void, failure: @escaping (Error) -> Void) {
        findPrebidCreativeBidder(adObject) { (bidder) in
            if let bidder = bidder {
                Log.debug("bidder:\(bidder)")
                success(bidder)
            } else {
                failure(NSError(domain: "", code:0, userInfo: [NSLocalizedDescriptionKey : "Can not get bidder"]))
            }
        }
    }
    
    @objc public func findPrebidCreativeBannerBidder(_ adView: UIView, success: @escaping (String) -> Void, failure: @escaping (Error) -> Void) {
        findPrebidCreativeBannerBidder(adView) { (bidder) in
            if let bidder = bidder {
                Log.debug("bidder:\(bidder)")
                
                success(bidder)
            } else {
                failure(NSError(domain: "", code:0, userInfo: [NSLocalizedDescriptionKey : "Can not get banner bidder"]))
            }
        }
    }
    
    @objc public func findPrebidCreativeSize(_ adView: UIView, success: @escaping (CGSize) -> Void, failure: @escaping (Error) -> Void) {
        findPrebidCreativeSize(adView) { (size) in
            if let size = size {
                Log.debug("size:\(size)")
                
                success(size)
            } else {
                failure(NSError(domain: "", code:0, userInfo: [NSLocalizedDescriptionKey : "Can not get size"]))
            }
        }
    }
    
    public func findPrebidCreativeBidder(_ adObject: NSObject, completion: @escaping (String?) -> Void) {
        
        let error = tryBlock {
            let webview: Any?
            if GADMobileAds.sharedInstance().isSDKVersionAtLeast(major: 8, minor: 0, patch: 0) {
                webview = adObject.value(forKeyPath: .GAD_WEBVIEW_PATH_8_0_0)
            } else if GADMobileAds.sharedInstance().isSDKVersionAtLeast(major: 7, minor: 59, patch: 0) {
                webview = adObject.value(forKeyPath: .GAD_WEBVIEW_PATH_7_59_0)
            } else {
                webview = adObject.value(forKeyPath: .GAD_WEBVIEW_PATH)
            }
            if let webview = webview as? UIView {
                if let wkWebView = webview as? WKWebView  {
                    Log.debug("subView is WKWebView")
                    self.findBidderInWebViewAsync(wkWebView: wkWebView, completion: completion)
                    
                } else {
                    Log.warn("subView doesn't include WebView")
                    completion(nil)
                }
            } else {
                Log.debug("Couldn't find webview")
                completion(nil)
            }
        }
        
        if (error != nil) {
            Log.debug("ERROR: Unknown error: \(String(describing: error))")
            completion(nil)
        }
    }
    
    public func findPrebidCreativeBannerBidder(_ adView: UIView, completion: @escaping (String?) -> Void) {
        
        let view = self.recursivelyFindWebView(adView) { (subView) -> Bool in
            return subView is WKWebView
        }
        
        if let wkWebView = view as? WKWebView  {
            Log.debug("subView is WKWebView")
            self.findBidderInWebViewAsync(wkWebView: wkWebView, completion: completion)
            
        } else {
            Log.warn("subView doesn't include WebView")
            completion(nil)
        }
       
    }
    
//    @available(iOS, deprecated, message: "Please migrate to - findPrebidCreativeSize(_:success:failure:)")
    public func findPrebidCreativeSize(_ adView: UIView, completion: @escaping (CGSize?) -> Void) {
        
        let view = self.recursivelyFindWebView(adView) { (subView) -> Bool in
            return subView is WKWebView
        }
        
        if let wkWebView = view as? WKWebView  {
            Log.debug("subView is WKWebView")
            self.findSizeInWebViewAsync(wkWebView: wkWebView, completion: completion)
            
        } else {
            Log.warn("subView doesn't include WebView")
            completion(nil)
        }
       
    }
    
    func runBidderCompletion(bidder: String?, completion: @escaping (String?) -> Void) {
        
        completion(bidder)
    }
    
    func runResizeCompletion(size: CGSize?, completion: @escaping (CGSize?) -> Void) {
        
        completion(size)
    }
    
    func recursivelyFindWebView(_ view: UIView, closure:(UIView) -> Bool) -> UIView? {
        for subview in view.subviews {
            
            if closure(subview)  {
                return subview
            }
            
            if let result = recursivelyFindWebView(subview, closure: closure) {
                return result
            }
        }
        
        return nil
    }
    
    func findBidderInWebViewAsync(wkWebView: WKWebView, completion: @escaping (String?) -> Void) {
        
        wkWebView.evaluateJavaScript("document.body.innerHTML", completionHandler: { (value: Any!, error: Error!) -> Void in
            
            if error != nil {
                Log.warn("error:\(error.localizedDescription)")
                return
            }

            let wkResult = self.findBidderInJavaScript(jsCode: value as? String)
            
            self.runBidderCompletion(bidder: wkResult, completion: completion)
        })
        
    }
    
    func findSizeInWebViewAsync(wkWebView: WKWebView, completion: @escaping (CGSize?) -> Void) {
        
        wkWebView.evaluateJavaScript("document.body.innerHTML", completionHandler: { (value: Any!, error: Error!) -> Void in
            
            if error != nil {
                Log.warn("error:\(error.localizedDescription)")
                return
            }

            let wkResult = self.findSizeInJavaScript(jsCode: value as? String)
            
            self.runResizeCompletion(size: wkResult, completion: completion)
        })
        
    }
    
    func findBidderInJavaScript(jsCode: String?) -> String? {
        guard let jsCode = jsCode else {
            Log.warn("jsCode is nil")
            return nil
        }
        
        guard let hbBidderKey = findHbBidderKey(in: jsCode) else {
            Log.warn("HbBidderKey is nil")
            return nil
        }
        return hbBidderKey
    }
    
    func findSizeInJavaScript(jsCode: String?) -> CGSize? {
        guard let jsCode = jsCode else {
            Log.warn("jsCode is nil")
            return nil
        }
        
        guard let hbSizeKeyValue = findHbSizeKeyValue(in: jsCode) else {
            Log.warn("HbSizeKeyValue is nil")
            return nil
        }
            
        guard let hbSizeValue = findHbSizeValue(in: hbSizeKeyValue) else {
            Log.warn("HbSizeValue is nil")
            return nil
        }
        
        return stringToCGSize(hbSizeValue)
    }
    
    func findHbBidderKey(in text: String) -> String? {
        return matchAndCheck(regex: "hb_bidder", text: text)
    }
    
    func findHbSizeKeyValue(in text: String) -> String?{
        return matchAndCheck(regex: "hb_size\\W+[0-9]+x[0-9]+", text: text)
    }
    
    func findHbSizeValue(in hbSizeKeyValue: String) -> String?{
        return matchAndCheck(regex: "[0-9]+x[0-9]+", text: hbSizeKeyValue)
    }
    
    func matchAndCheck(regex: String, text: String) -> String?{
        let matched = matches(for: regex, in: text)
        
        if matched.isEmpty {
            return nil
        }
        
        let firstResult = matched[0]
        
        return firstResult
    }
    
    func matches(for regex: String, in text: String) -> [String] {
        
        do {
            let regex = try NSRegularExpression(pattern: regex)
            let results = regex.matches(in: text, range: NSRange(text.startIndex..., in: text))
            return results.map {
                String(text[Range($0.range, in: text)!])
            }
        } catch let error {
            Log.warn("invalid regex: \(error.localizedDescription)")
            return []
        }
    }
    
    func stringToCGSize(_ size: String) -> CGSize? {
        
        let sizeArr = size.split{$0 == "x"}.map(String.init)
        guard sizeArr.count == 2 else {
            Log.warn("\(size) has a wrong format")
            return nil
        }
        
        let nsNumberWidth = NumberFormatter().number(from: sizeArr[0])
        let nsNumberHeight = NumberFormatter().number(from: sizeArr[1])
        
        guard let numberWidth = nsNumberWidth, let numberHeight = nsNumberHeight else {
            Log.warn("\(size) can not be converted to CGSize")
            return nil
        }
        
        let width = CGFloat(truncating: numberWidth)
        let height = CGFloat(truncating: numberHeight)
        
        let cgSize = CGSize(width: width, height: height)
        
        return cgSize
    }
    
    static func truncateDfpKeys(_ dfpKey: String) -> String {
        if (dfpKey.count > .DFP_KEY_MAX_LENGTH) {
            return String(dfpKey.prefix(.DFP_KEY_MAX_LENGTH))
        }
        return dfpKey
    }
    
    @objc func validateAndAttachKeywords (adObject: AnyObject, customKeywords: [String:String]) {
        let adServerObject: String = String(describing: type(of: adObject))
        if (adServerObject == .DFP_Object_Name || adServerObject == .DFP_O_Object_Name ||
            adServerObject == .DFP_N_Object_Name || adServerObject == .GAD_N_Object_Name ||
            adServerObject == .GAD_Object_Name || adServerObject == .GAM_Object_Name) {
            let hasDFPMember = adObject.responds(to: NSSelectorFromString("setCustomTargeting:"))
            if (hasDFPMember) {
                //check if the publisher has added any custom targeting. If so then merge the bid keywords to the same.
                if (adObject.value(forKey: "customTargeting") != nil) {
                    var existingDict: [String: Any] = adObject.value(forKey: "customTargeting") as! [String: Any]
                    existingDict.merge(dict: customKeywords)
                    adObject.setValue( existingDict, forKey: "customTargeting")
                } else {
                    adObject.setValue( customKeywords, forKey: "customTargeting")
                }

                return
            }
        }

        if (adServerObject == .MoPub_Object_Name || adServerObject == .MoPub_Interstitial_Name) {
            let hasMoPubMember = adObject.responds(to: NSSelectorFromString("setKeywords:"))

            if (hasMoPubMember) {
                //for mopub the keywords has to be set as a string seperated by ,
                // split the dictionary & construct a string comma separated
                var targetingKeywordsString: String = ""
                //get the publisher set keywords & append the bid keywords to the same

                if let keywordsString = (adObject.value(forKey: "keywords") as? String) {
                    targetingKeywordsString = keywordsString
                }

                let commaString: String = ","

                for (key, value) in customKeywords {
                    if ( targetingKeywordsString == .EMPTY_String) {
                        targetingKeywordsString = key + ":" + value
                    } else {
                        targetingKeywordsString += commaString + key + ":" + value
                    }

                }

                Log.info("MoPub targeting keys are \(targetingKeywordsString)")
                adObject.setValue( targetingKeywordsString, forKey: "keywords")

            }

        }
        
        if (adServerObject == .Smart_Admax_Adapter_Object_Name) {
            (adObject as? UpdatableProtocol)?.update(keywords: customKeywords)
        }
    }

    @objc func validateAndAttachKeywords (adObject: AnyObject, bidResponse: BidResponse) {
        self.validateAndAttachKeywords(adObject: adObject, customKeywords: bidResponse.customKeywords)
    }

    @objc
    public func findNative(adObject: AnyObject){
        if (self.isObjectFromClass(adObject, DFP_BANNER_VIEW_CLASSNAME)) {
            let dfpBannerView = adObject as! UIView
            findNativeForDFPBannerAdView(dfpBannerView)
        } else if (self.isObjectFromClass(adObject, DFP_CUSTOM_TEMPLATE_AD_CLASSNAME) || self.isObjectFromClass(adObject, GAD_CUSTOM_NATIVE_AD)) {
            findNativeForDFPCustomTemplateAd(adObject)
        } else {
            delegate?.nativeAdNotFound()
        }
    }

    private func findNativeForDFPCustomTemplateAd(_ dfpCustomAd: AnyObject){
        let isPrebid = dfpCustomAd.string?(forKey: "isPrebid")
        if("1" == isPrebid) {
            if let hb_cache_id_local = dfpCustomAd.string?(forKey: "hb_cache_id_local"), CacheManager.shared.isValid(cacheId: hb_cache_id_local)
            {
                let ad = NativeAd.create(cacheId: hb_cache_id_local)
                if (ad != nil) {
                    delegate?.nativeAdLoaded(ad: ad!)
                    return
                } else {
                    delegate?.nativeAdNotValid()
                    return
                }
            }
        }

        delegate?.nativeAdNotFound()
    }
    
    private func isObjectFromClass(_ object: AnyObject, _ className: String) -> Bool{
        let objectClassName = String(describing: type(of: object))
        if objectClassName == className {
            return true
        }
        return false
    }
    
    private func findNativeForDFPBannerAdView(_ view:UIView){
        var array = [UIView]()
        recursivelyFindWebViewList(view, &array)
        if array.count == 0 {
            delegate?.nativeAdNotFound()
        } else {
            self.iterateWebViewListAsync(array, array.count - 1)
        }

    }

    private func iterateWebViewListAsync(_ array: [UIView], _ index: Int){
        let processNextWebView:(Int)->Void = {(i) in
            if i > 0 {
                self.iterateWebViewListAsync(array, i - 1)
            } else {
                self.delegate?.nativeAdNotFound()
            }
        }
        let processHTMLContent:(String)->Void = {(html) in
            if let cacheId = self.getCacheIdFromBody(html), CacheManager.shared.isValid(cacheId: cacheId) {
                let ad = NativeAd.create(cacheId: cacheId)
                if ad != nil {
                    self.delegate?.nativeAdLoaded(ad: ad!)
                } else {
                    self.delegate?.nativeAdNotValid()
                }
            } else {
                processNextWebView(index)
            }
        }
        let gadWebAdView = array[index]
        let controller = gadWebAdView.value(forKey: "webViewController")
        let someController = controller as! NSObject
        let webView = someController.value(forKey: "webView") as! UIView
        if webView is WKWebView {
            let wk = webView as! WKWebView
            wk.evaluateJavaScript(self.INNNER_HTML_SCRIPT, completionHandler: { (value: Any!, error: Error!) -> Void in

                if error != nil {
                    return
                }

                let html = value as! String
                processHTMLContent(html)
            })
        } else {
            processNextWebView(index)
        }
    }

    private func getCacheIdFromBody(_ body: String) -> String? {
        let regex = "\\%\\%Prebid\\%\\%.*\\%\\%Prebid\\%\\%"
        do {
            let regex = try NSRegularExpression(pattern: regex)
            let results = regex.matches(in: body, range: NSRange(body.startIndex..., in: body))
            let matched = results.map {
                String(body[Range($0.range, in: body)!])
            }
            if matched.isEmpty {
                return nil
            }
            let firstResult = matched[0]

            return firstResult
        } catch {
            return nil
        }
    }

    private func recursivelyFindWebViewList(_ view:UIView, _ webViewArray:inout [UIView]){
        if(self.isObjectFromClass(view, self.DFP_WEBADVIEW_CLASSNAME)){
            webViewArray.append(view)
        } else {
            for subview in view.subviews {
                recursivelyFindWebViewList(subview, &webViewArray)
            }
        }
    }
    
    func getStringFromDictionary(_ dic: [String:AnyObject]) -> String? {
        do {
            let jsonData = try JSONSerialization.data(withJSONObject: dic, options: [])
            let text = String(data: jsonData, encoding: .utf8)
            return text
        } catch {
            print(error.localizedDescription)
        }
        return nil
    }

    func getDictionaryFromString(_ text: String) -> [String:AnyObject]? {
        if let data = text.data(using: .utf8) {
            do {
                let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String:AnyObject]
                return json
            } catch {
                print(error.localizedDescription)
            }
        }
        return nil
    }

}

public protocol UpdatableProtocol {
    func update(keywords: [String: String])
}
