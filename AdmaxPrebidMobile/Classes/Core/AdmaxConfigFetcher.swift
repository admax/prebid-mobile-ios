//
//  AdmaxConfigFetcher.swift
//  AdmaxPrebidMobile
//
//  Created by Gwen on 25/03/2020.
//

import Foundation

class AdmaxConfigFetcher {
    
    var admaxConfig: Data? {
        didSet {
            writeAdmaxConfigToStorage(admaxConfig: admaxConfig)
        }
    }
    var refreshingAdmaxConfig: Bool = false
    
    init() {
        admaxConfig = readAdmaxConfigFromStorage()
        refreshAdmaxConfig()
    }
    
    func refreshAdmaxConfig() {
        if refreshingAdmaxConfig {
            return
        }
        refreshingAdmaxConfig = true
        refreshAdmaxConfigAsync()
    }
    
    func readAdmaxConfigFromStorage() -> Data? {
        let admaxConfig = UserDefaults.standard.object(forKey: .User_Defaults_Admax_Config) as? Data
        return admaxConfig
    }
    
    func writeAdmaxConfigToStorage(admaxConfig: Data?) {
        UserDefaults.standard.set(admaxConfig, forKey: .User_Defaults_Admax_Config)
    }
    
    func refreshAdmaxConfigAsync() {
        Log.info("Fetching admax config from url \(.Admax_Config_Endpoint + Prebid.shared.prebidServerAccountId)")
        var request: URLRequest = URLRequest(url: URL(string: .Admax_Config_Endpoint + Prebid.shared.prebidServerAccountId)!, cachePolicy: .reloadIgnoringLocalCacheData, timeoutInterval: TimeInterval(Prebid.shared.admaxAuctionTimeoutMs))
        request.httpMethod = "GET"
        let demandFetchStartTime = self.getCurrentMillis()
        URLSession.shared.dataTask(with: request) { (data, _, error) in
            let demandFetchEndTime = self.getCurrentMillis()
            let demandFetchElapsedTime = demandFetchEndTime - demandFetchStartTime
            Log.info("Admax Config fetch Response Time in milliseconds: \(demandFetchElapsedTime)")
            defer {
                self.refreshingAdmaxConfig = false
            }
            guard error == nil else {
                Log.error("error calling GET on \(.Admax_Config_Endpoint + Prebid.shared.prebidServerAccountId)")
                return
            }
            if let data = data {
                self.admaxConfig = data
            } else {
                Log.debug("No Data found in http response")
            }
            
        }.resume()
    }
    
    func getCurrentMillis() -> Int64 {
        return Int64(Date().timeIntervalSince1970 * 1000)
    }
}
