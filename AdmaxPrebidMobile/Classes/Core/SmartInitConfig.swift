//
//  SmartInitConfig.swift
//  AdmaxPrebidMobile
//
//  Created by Gwen on 15/04/2020.
//

import Foundation

class SmartInitConfig {
    
    var siteId: Int
    
    init(siteId: Int) {
        self.siteId = siteId
    }
}
