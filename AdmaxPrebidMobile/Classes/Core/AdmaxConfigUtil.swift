//
//  File.swift
//  AdmaxPrebidMobile
//
//  Created by Gwen on 25/03/2020.
//

import Foundation

public class AdmaxConfigUtil {
    public static func getKeyvaluePrefix(admaxConfig: Data?) -> String {
        if let admaxConfig = admaxConfig, let admaxConfigJSON = try? JSONSerialization.jsonObject(with: admaxConfig, options: []) as? [String:Any] {
            if let keyvaluePrefix = admaxConfigJSON[.Keyvalue_Prefix] as? String {
                return keyvaluePrefix
            } else {
                Log.debug("Keyvalue prefix not set, using default hb_pb")
            }
        } else {
            Log.error("Error Serializing JSON admaxConfig")
        }
        return .Default_Keyvalue_Prefix
    }
    
    public static func getPriceGranularityConfig(admaxConfig: Data?) -> PriceGranularityConfig {
        if let admaxConfig = admaxConfig, let admaxConfigJSON = try? JSONSerialization.jsonObject(with: admaxConfig, options: []) as? [String: Any] {
            if let admaxPGConfigJSON = admaxConfigJSON[.PriceGranularity] as? [String:Any] {
                if let precision = admaxPGConfigJSON[.Pg_Precision] as? Int, let pgRanges = admaxPGConfigJSON[.Pg_Ranges] as? [AnyObject] {
                    var ranges: [PriceGranularityRange] = [PriceGranularityRange]()
                    for pgRange in pgRanges {
                        var min: Double = 0
                        var max: Double = 0
                        var increment: Double = 0
                        if let pgRangeMin = pgRange[String.Pg_Ranges_Min] as? Double {
                            min = pgRangeMin
                        }
                        if let pgRangeMax = pgRange[String.Pg_Ranges_Max] as? Double {
                            max = pgRangeMax
                        }
                        if let pgRangeIncr = pgRange[String.Pg_Ranges_Incr] as? Double {
                            increment = pgRangeIncr
                        }
                        if (increment > 0 && max > 0) {
                            ranges.append(PriceGranularityRange(min: min, max: max, increment: increment))
                        } else {
                            return .Default_Pg_Config
                        }
                    }
                    return PriceGranularityConfig(precision: precision, ranges: ranges)
                }
            } else {
                Log.debug("Price granularity configs missing")
            }
        } else {
            Log.debug("admaxConfig JSON serialization failed")
        }
        return .Default_Pg_Config
    }
    
    static func getClientAuctionTimeoutMs(admaxConfig: Data?) -> Int {
        if let admaxConfig = admaxConfig, let admaxConfigJSON = try? JSONSerialization.jsonObject(with: admaxConfig, options: []) as? [String:Any] {
            if let clientAuctionTimeout = admaxConfigJSON[.Client_Auction_Timeout] as? Int {
                return clientAuctionTimeout
            } else {
                Log.debug("Client auction timeout not set")
            }
        } else {
            Log.error("Error Serializing JSON admaxConfig")
        }
        return .Default_Client_Auction_Timeout
    }
    
    static func getAdmaxAuctionTimeoutMs(admaxConfig: Data?) -> Int {
        if let admaxConfig = admaxConfig, let admaxConfigJSON = try? JSONSerialization.jsonObject(with: admaxConfig, options: []) as? [String:Any] {
            if let admaxAuctionTimeout = admaxConfigJSON[.Admax_Auction_Timeout] as? Int {
                return admaxAuctionTimeout
            } else {
                Log.debug("Admax auction timeout not set")
            }
        } else {
            Log.error("Error Serializing JSON admaxConfig")
        }
        return .Default_Admax_Auction_Timeout
    }
    
    static func getBidderConfig(admaxConfig: Data?, admaxConfigId: String, bidder: String) -> [String:Any]? {
        if let admaxConfig = admaxConfig, let admaxConfigJSON = try? JSONSerialization.jsonObject(with: admaxConfig, options: []) as? [String:Any] {
            if let admaxAdunitsConfigJSON = admaxConfigJSON[.Config_Adunits] as? [String:Any] {
                if let adunitConfigJSON = admaxAdunitsConfigJSON[admaxConfigId] as? [String:Any] {
                    if let adunitBidderConfigJSON = adunitConfigJSON[bidder] as? [String:Any] {
                        return adunitBidderConfigJSON
                    } else {
                        Log.debug("No config for bidder \(bidder)")
                    }
                } else {
                    Log.debug("No config for Admax adunit config \(admaxConfigId)")
                }
            } else {
                Log.debug("Admax adunits configs missing")
            }
        } else {
            Log.debug("admaxConfig is empty")
        }
        return nil
    }
    
    static func getSmartConfigs(admaxConfig: Data?) -> SmartInitConfig? {
        if let admaxConfig = admaxConfig, let admaxConfigJSON = try? JSONSerialization.jsonObject(with: admaxConfig, options: []) as? [String: Any] {
            if let admaxAdunitsConfigJSON = admaxConfigJSON[.Config_Adunits] as? [String:Any] {
                let configIds = admaxAdunitsConfigJSON.keys
                for configId in configIds {
                    let adunitConfigJSON = admaxAdunitsConfigJSON[configId] as! [String:Any]
                    if let smartConfigJSON = adunitConfigJSON[.Smart_Bidder] as? [String:Any] {
                        if let smartSiteId = smartConfigJSON[.Smart_Site_Id] as? Int {
                            return SmartInitConfig(siteId: smartSiteId)
                        }
                    }
                }
            } else {
                Log.debug("Admax adunits configs missing")
            }
        } else {
            Log.debug("admaxConfig JSON serialization failed")
        }
        return nil
    }
}
