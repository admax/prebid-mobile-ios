//
//  PriceGranularityConfig.swift
//  AdmaxPrebidMobile
//
//  Created by Ralph Romanos on 01/12/2021.
//

import Foundation

public class PriceGranularityConfig {
    private var precision: Int = 2
    private var ranges: Array<PriceGranularityRange>
    
    public init(precision: Int, range: PriceGranularityRange) {
        self.precision = precision
        self.ranges = Array<PriceGranularityRange>()
        self.ranges.append(range)
    }
    
    public init(precision: Int, ranges: Array<PriceGranularityRange>) {
        self.precision = precision
        self.ranges = ranges
    }
    
    public func getPriceBucket(cpm: Double) -> String {
        var cpmStr: String = ""
        var bucketMax: Double = 0
        var increment: Double = 0
        let precision: Int = self.precision
        for range in ranges {
            if (range.max > bucketMax) {
                bucketMax = range.max
            }
            if (cpm >= range.min && cpm <= range.max) {
                increment = range.increment
            }
        }
        if (cpm > bucketMax) {
            cpmStr = String(format: "%.\(precision)f", bucketMax)
        } else if (increment > 0) {
            cpmStr = getCpmTarget(cpm: cpm, increment: increment, precision: precision)
        }
        return cpmStr
    }
    
    func getCpmTarget(cpm: Double, increment: Double, precision: Int) -> String {
        let roundedCPM = floor(cpm * 100 / increment / 100) * increment
        return String(format: "%.\(precision)f", roundedCPM)
    }
}
