//
//  PriceGranularityRange.swift
//  AdmaxPrebidMobile
//
//  Created by Ralph Romanos on 01/12/2021.
//

import Foundation

public class PriceGranularityRange {
    var min: Double
    var max: Double
    var increment: Double
    
    public init(min: Double, max: Double, increment: Double) {
        self.min = min
        self.max = max
        self.increment = increment
    }
}
