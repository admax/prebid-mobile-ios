/*   Copyright 2018-2019 Prebid.org, Inc.

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 */

import Foundation
import SASDisplayKit
import GoogleMobileAds

@objcMembers public class Prebid: NSObject {
    public var admaxAuctionTimeoutMs: Int = .Default_Admax_Auction_Timeout
    public var clientAuctionTimeoutMs: Int = .Default_Client_Auction_Timeout
    var timeoutUpdated: Bool = false
    
    public var loggingEnabled: Bool = false

    public var prebidServerAccountId: String! = ""
    
    public var admaxConfig: Data?
    /**
    * This property is set by the developer when he is willing to share the location for better ad targeting
    **/
    private var geoLocation: Bool = false
    public var shareGeoLocation: Bool {
        get {
           return geoLocation
        }

        set {
            geoLocation = newValue
            if (geoLocation == true) {
                Location.shared.startCapture()
            } else {
                Location.shared.stopCapture()
            }
        }
    }
    
    public var admaxExceptionLogger: AdmaxExceptionLogger?

    public var prebidServerHost: PrebidHost = PrebidHost.Admax

    /**
     * The class is created as a singleton object & used
     */
    public static let shared = Prebid()

    /**
     * The initializer that needs to be created only once
     */
    private override init() {
        super.init()
        if (RequestBuilder.myUserAgent == "") {
            RequestBuilder.UserAgent {(userAgentString) in
                RequestBuilder.myUserAgent = userAgentString
            }
        }
    }

    public func setCustomPrebidServer(url: String) throws {

        if (Host.shared.verifyUrl(urlString: url) == false) {
                throw ErrorCode.prebidServerURLInvalid(url)
        } else {
            prebidServerHost = PrebidHost.Custom
            Host.shared.setHostURL = url
        }
    }
    
    public func initAdmaxConfig() {
        GADMobileAds.sharedInstance().applicationVolume = 0
        GADMobileAds.sharedInstance().applicationMuted = true
        if !prebidServerAccountId.isEmpty {
            let configFetcher = AdmaxConfigFetcher()
            admaxConfig = configFetcher.admaxConfig
            admaxAuctionTimeoutMs = AdmaxConfigUtil.getAdmaxAuctionTimeoutMs(admaxConfig: admaxConfig)
            clientAuctionTimeoutMs = AdmaxConfigUtil.getClientAuctionTimeoutMs(admaxConfig: admaxConfig)
            Log.debug("admaxAuctionTimeoutMs: \(admaxAuctionTimeoutMs)")
            Log.debug("clientAuctionTimeoutMs: \(clientAuctionTimeoutMs)")
            if let smartConfig = AdmaxConfigUtil.getSmartConfigs(admaxConfig: admaxConfig) {
                initSmart(smartConfig: smartConfig)
            }
        } else {
            Log.error("Admax accountId needs to be set using prebidServerAccountId before calling initAdmaxConfig")
        }
    }
    
    private func initSmart(smartConfig: SmartInitConfig) {
        if (!SASConfiguration.shared.isConfigured) {
            SASConfiguration.shared.configure(siteId: smartConfig.siteId)
            SASConfiguration.shared.allowAutomaticLocationDetection = true
        }
    }
}
