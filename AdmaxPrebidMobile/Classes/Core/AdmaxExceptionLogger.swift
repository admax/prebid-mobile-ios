//
//  AdmaxExceptionLogger.swift
//  PrebidMobile
//
//  Created by Gwen on 05/06/2019.
//  Copyright © 2019 AppNexus. All rights reserved.
//

import Foundation

@objc public protocol AdmaxExceptionLogger {
    func logException(cause: Error)
}
