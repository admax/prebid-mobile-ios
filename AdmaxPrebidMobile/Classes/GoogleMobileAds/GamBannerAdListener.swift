//
//  GamBannerAdListener.swift
//  PrebidMobile
//
//  Created by Gwen on 08/01/2020.
//  Copyright © 2020 Admax. All rights reserved.
//

import Foundation
import GoogleMobileAds

@objc public protocol GamBannerAdListener {
    func onAdLoaded(bannerUnit: GamBannerAdUnit, size: GADAdSize)
    func onAdFailedToLoad(error: Error)
}
