/*   Copyright 2018-2019 Prebid.org, Inc.

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 */

import Foundation
import GoogleMobileAds

@objcMembers public class GamInterstitialAdUnit: InterstitialAdUnit, GADFullScreenContentDelegate {
    
    weak var gamAdListener: GamInterstitialAdListener?
    
    var dfpInterstitial: GAMInterstitialAd!
    
    var gamAdUnitId: String = ""
    
    public func setGamInterstitialAdListener(adListener: GamInterstitialAdListener) {
        self.gamAdListener = adListener
    }
    
    public func getGamInterstitialAdListener() -> GamInterstitialAdListener? {
        return self.gamAdListener
    }
    
    public func createDfpOnlyInterstitial() {
        let gamRequest = GAMRequest()
        GAMInterstitialAd.load(withAdManagerAdUnitID: getGamAdUnitId(), request: gamRequest) { [weak self] (ad, error) in
            if let error = error, let self = self {
                Log.info("Failed to load interstitial ad with error: \(error.localizedDescription)")
                if self.gamAdListener != nil {
                    self.gamAdListener?.onAdFailedToLoad(error: error)
                }
            } else if let ad = ad, let self = self, let rootViewController = self.rootViewController {
                self.dfpInterstitial = ad
                self.dfpInterstitial.fullScreenContentDelegate = self
                self.dfpInterstitial.present(fromRootViewController: rootViewController)
                if self.gamAdListener != nil {
                    self.gamAdListener?.onAdLoaded(interstitialUnit: self)
                }
            }
        }
    }
    
    public func setGamAdUnitId(gamAdUnitId: String) {
        self.gamAdUnitId = gamAdUnitId
    }
    
    public func getGamAdUnitId() -> String {
        return self.gamAdUnitId
    }
    
    public func adDidDismissFullScreenContent(_ ad: GADFullScreenPresentingAd) {
        Log.info("Ad dismissed")
        if gamAdListener != nil {
            gamAdListener?.onAdDidDismissScreen(ad)
        }
    }
    
    public func ad(_ ad: GADFullScreenPresentingAd, didFailToPresentFullScreenContentWithError error: Error) {
        Log.info("Ad did fail to present full screen")
        if gamAdListener != nil {
            gamAdListener?.onAdDidFail(toPresentScreen: ad)
        }
    }
}
