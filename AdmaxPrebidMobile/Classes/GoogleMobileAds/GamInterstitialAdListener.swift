//
//  GamInterstitialAdListener.swift
//  AdmaxPrebidMobile
//
//  Created by Gwen on 15/12/2020.
//

import Foundation
import GoogleMobileAds

@objc public protocol GamInterstitialAdListener {
    func onAdLoaded(interstitialUnit: GamInterstitialAdUnit)
    func onAdFailedToLoad(error: Error)
    func onAdDidDismissScreen(_ ad: GADFullScreenPresentingAd)
    func onAdDidFail(toPresentScreen ad: GADFullScreenPresentingAd)
}
