/*   Copyright 2018-2019 Prebid.org, Inc.

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 */
    
import Foundation
import GoogleMobileAds

@objcMembers public class GamBannerAdUnit: BannerAdUnit, GADBannerViewDelegate{
    
    weak var gamAdListener: GamBannerAdListener?
    
    var gamSizes = Array<NSValue> ()
    
    var gamAdUnitId: String = ""

    public override init(configId: String, size: CGSize, viewController: UIViewController?, adContainer: UIView?) {
        super.init(configId: configId, size: size, viewController: viewController, adContainer: adContainer)
        gamSizes.append(NSValueFromGADAdSize(GADAdSizeFromCGSize(size)))
    }

     override public func addAdditionalSize(sizes: [CGSize]) {
        super.addAdditionalSize(sizes: sizes)
        for size in sizes {
            gamSizes.append(NSValueFromGADAdSize(GADAdSizeFromCGSize(size)))
        }
    }
    
    public func addAdditionalGamSize(size: GADAdSize) {
        gamSizes.append(NSValueFromGADAdSize(size))
    }
    
    public func setGamBannerAdListener(adListener: GamBannerAdListener) {
        self.gamAdListener = adListener
    }
    
    public func getGamBannerAdListener() -> GamBannerAdListener? {
        return self.gamAdListener
    }
    
    public func createDFPOnlyBanner() {
        if let adContainer = adContainer {
            let dfpBannerView: GAMBannerView = GAMBannerView()
            dfpBannerView.validAdSizes = gamSizes
            dfpBannerView.adUnitID = self.getGamAdUnitId()
            dfpBannerView.rootViewController = rootViewController
            dfpBannerView.delegate = self
            for subview in adContainer.subviews {
                subview.removeFromSuperview()
            }
            adContainer.addSubview(dfpBannerView)
            let dfpRequest : GAMRequest = GAMRequest()
            dfpBannerView.load(dfpRequest)
        } else {
            Log.debug("adContainer has not been initialized on this AdUnit")
        }
    }
    
    public func setGamAdUnitId(gamAdUnitId: String) {
        self.gamAdUnitId = gamAdUnitId
    }
    
    public func getGamAdUnitId() -> String {
        return self.gamAdUnitId
    }
    
    public func bannerViewDidReceiveAd(_ bannerView: GADBannerView) {
        if gamAdListener != nil {
            gamAdListener?.onAdLoaded(bannerUnit: self, size: bannerView.adSize)
        }
    }
    
    public func bannerView(_ bannerView: GADBannerView, didFailToReceiveAdWithError error: Error) {
        if gamAdListener != nil {
            gamAdListener?.onAdFailedToLoad(error: error)
        }
    }

}
